package com.mySql.musicstore;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mySql.musicstore.entities.Album;
import com.mySql.musicstore.entities.Artist;
import com.mySql.musicstore.entities.Customer;
import com.mySql.musicstore.entities.User;
import com.mySql.musicstore.jpa.StoreDataService;

@RestController
public class MusicStoreController {

    @Autowired
    StoreDataService dataService;

    @CrossOrigin
    @PostMapping("/customers")
    public Customer addCustomer(@RequestBody Customer customer) {
	Customer cust = dataService.addCustomer(customer);
	return cust;

    }

    @CrossOrigin
    @PostMapping("/artists")
    public void addArtist(@RequestBody Artist artist) {
	dataService.addArtist(artist);
    }

    @CrossOrigin
    @PostMapping("/users")
    public User saveUser(@RequestBody @Valid User user) {
	dataService.addUser(user);
	User newUser = dataService.addUser(user);
	return newUser;
    }

    @CrossOrigin
    @GetMapping("/users")
    @ResponseBody
    public Collection<User> getUsers() {
	Collection<User> users = dataService.getUsers();
	return users;
    }

    @CrossOrigin
    @GetMapping("/artists")
    @ResponseBody
    public Collection<Artist> getArtists() {
	return dataService.getArtists();
    }

    @CrossOrigin
    @GetMapping("/artists/firstName/{firstName}")
    @ResponseBody
    public Collection<Artist> getArtistByFirstName(@PathVariable String firstName) {
	return dataService.getArtistsByFirstName(firstName);
    }

    @CrossOrigin
    @GetMapping("/artists/id/{id}")
    @ResponseBody
    public Artist getArtistById(@PathVariable long id) {
	return dataService.getArtistsById(id);
    }

    @CrossOrigin
    @GetMapping("/albums")
    @ResponseBody
    public Collection<Album> getAlbums() {
	return dataService.getAlbums();
    }

    @CrossOrigin
    @ResponseBody
    @GetMapping("/customers")
    public List<Customer> getCustomers() {
	List<Customer> custs = dataService.getCustomers();
	return custs;

    }

    @CrossOrigin
    @ResponseBody
    @GetMapping("/customer/firstName/{fName}")
    public List<Customer> getCustomersByFirstName(@PathVariable("fName") String fName) {
	List<Customer> custs = dataService.getCustomerByFirstName(fName);
	return custs;

    }

}