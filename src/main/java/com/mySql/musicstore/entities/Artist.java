package com.mySql.musicstore.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Artist {

    @GeneratedValue
    @Id
    private long id;

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String stageName;
    @NotNull
    private String label;

    @OneToMany(mappedBy = "artist", fetch = FetchType.EAGER)
    private List<Album> albums;

    public Artist() {
	// TODO Auto-generated constructor stub
    }

    public Artist(@NotNull String firstName, @NotNull String lastName, @NotNull String stageName,
	    @NotNull String label) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.stageName = stageName;
	this.label = label;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getStageName() {
	return stageName;
    }

    public void setStageName(String stageName) {
	this.stageName = stageName;
    }

    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

}
