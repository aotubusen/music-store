package com.mySql.musicstore.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class User {

    @GeneratedValue
    @Id
    private long id;

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String email;
    @NotNull
    private LocalDate dateAdded;
    @NotNull
    private int age;
    private String imageUrl;

    public User() {
    }

    public User(String firstName, String lastName, String email, LocalDate dateAdded, int age, String imageUrl) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
	this.age = age;
	this.setDateAdded(dateAdded);
	this.imageUrl = imageUrl;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public LocalDate getDateAdded() {
	return dateAdded;
    }

    public void setDateAdded(LocalDate dateAdded) {
	this.dateAdded = dateAdded;
    }

    public String getImageUrl() {
	return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public int getAge() {
	return age;
    }

    public void setAge(int age) {
	this.age = age;
    }

}
