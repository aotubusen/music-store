package com.mySql.musicstore.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Album {

	@GeneratedValue
	@Id
	private long id;

	@ManyToOne
	private Artist artist;

	@NotNull
	private String name;
	@NotNull
	private int numberOfTracks;

	@NotNull
	private String label;

	public Album() {
		// TODO Auto-generated constructor stub
	}

	public Album(@NotNull String name, @NotNull int numberOfTracks, @NotNull String label) {
		super();
		this.name = name;
		this.numberOfTracks = numberOfTracks;
		this.label = label;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfTracks() {
		return numberOfTracks;
	}

	public void setNumberOfTracks(int numberOfTracks) {
		this.numberOfTracks = numberOfTracks;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
