package com.mySql.musicstore.jpa;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mySql.musicstore.entities.Album;
import com.mySql.musicstore.entities.Artist;
import com.mySql.musicstore.entities.Customer;
import com.mySql.musicstore.entities.User;

@Repository
@Transactional
public class StoreDataService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    UserRepository userRepository;

    private void emTest(String qry) {
	// em.getTransaction().begin();
	// em.getTransaction().commit();
	//
	// em.createNamedQuery(qry);
	// em.createNamedStoredProcedureQuery(qry);
	// em.getCriteriaBuilder().createQuery()
	// em.createStoredProcedureQuery(name);
    }

    public void addArtist(Artist artist) {
	em.persist(artist);
    }

    public Collection<Artist> getArtists() {
	Query q = em.createQuery("from Artist");
	List list = q.getResultList();
	return list;
    }

    public Artist getArtistsById(long id) {
	return em.find(Artist.class, id);
    }

    public Collection<Artist> getArtistsByFirstName(String firstName) {
	Query q = em.createQuery("from Artist where firstName = :firstName");
	q.setParameter("firstName", firstName);
	return q.getResultList();
    }

    public Collection<Album> getAlbums() {
	Query q = em.createQuery("from Album");
	List list = q.getResultList();
	return list;
    }

    public Customer addCustomer(Customer customer) {
	Customer cust = customerRepository.save(customer);
	return cust;

    }

    @SuppressWarnings("unchecked")
    public List<Customer> getCustomers() {
	@SuppressWarnings("unchecked")
	List<Customer> custs = em.createStoredProcedureQuery("get_all_customers").getResultList();
	// List<Customer> custs = customerRepository.findAll();
	return custs;

    }

    @SuppressWarnings("unchecked")
    public List<Customer> getCustomerByFirstName(String firstName) {
	@SuppressWarnings("unchecked")
	StoredProcedureQuery q = em.createStoredProcedureQuery("getCustomerByFirstName");
	q.registerStoredProcedureParameter("firstName", String.class, ParameterMode.IN);
	List<Customer> custs = q.setParameter("firstName", firstName).getResultList();
	return custs;

    }

    public User addUser(@Valid User user) {
	return userRepository.save(user);

    }

    public Collection<User> getUsers() {

	return userRepository.findAll();
    }

}
