package com.mySql.musicstore;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mySql.musicstore.entities.Artist;
import com.mySql.musicstore.jpa.StoreDataService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MusicstoreApplicationTests {

    @PersistenceContext
    EntityManager em;

    @Autowired
    StoreDataService storeDataService;

    @Transactional
    @Test
    public void setup() {
	em.persist(new Artist("Ade", "Otubusen", "JimmyD", "EMI"));
	em.persist(new Artist("Seert", "Otubusen1", "Sert", "EMI"));
	em.persist(new Artist("Porty", "HArray", "Arry", "EMI"));
	em.persist(new Artist("helo", "Hero", "SGERT", "EMI"));
    }

    @Transactional
    @Test
    public void contextLoads() {
	Collection<Artist> list = storeDataService.getArtistsByFirstName("Ade");
	assertTrue(list.isEmpty());
    }

    // @Transactional
    // @Test
    // public void test2() {
    // Collection<Artist> list = storeDataService.getArtistsByFirstName("Ade");
    // // assertTrue(list.isEmpty());
    // }

}
